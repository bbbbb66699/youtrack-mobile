export type Visibility = {
  $type: string,
  permittedUsers: Array<Object>,
  permittedGroups: Array<Object>,
}
